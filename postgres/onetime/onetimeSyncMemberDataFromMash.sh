#!/usr/bin/env sh

SOURCE_DB_HOST="dv-genesis-pg.cw6wax9ll7u5.us-east-1.rds.amazonaws.com"
TARGET_DB_HOST="localhost"
DB_USER="genesisuser"
DB_NAME="genesis"
TMP_DATA_DIRECTORY="/tmp"

export PGPASSWORD="a1111111"

SYNC_TABLES="trackers member_trackers pillar pillar_configuration i18n_pillar pillar_configuration_default_topic sponsor_pillar_settings i18n_sponsor_pillar_settings pillar_topic member_pillar_topic agreement i18n_agreement member_agreement segment_scheduled_checklist segment_feature segment_feature_bundle segment_device_promo segment_checklist segment_criteria segment_pillar_type segment_guide segment_onboarding_board segment_hra_provider segment_partner_subscription segment_program segment_redemption_brand segment_scheduled_promotion segment_scheduled_widget segment_sync_application segment_tag segment_criteria_group segment sponsor_settings sponsor company business_unit office member member_settings member_role member_role_access_level distributor"

for table_name in ${SYNC_TABLES}
do
    echo "dump table ${table_name} ..."
    pg_dump --disable-triggers -t ${table_name} --data-only -h ${SOURCE_DB_HOST} -Fc -o -U ${DB_USER} ${DB_NAME} -f ${TMP_DATA_DIRECTORY}/${table_name}.sql
    echo "restore table ${table_name} ..."
    pg_restore --disable-triggers -C ${TMP_DATA_DIRECTORY}/${table_name}.sql | psql -h ${TARGET_DB_HOST} -U ${DB_USER} -d ${DB_NAME}
done

rm -f /tmp/*.sql

echo "Completed data sync from mash to local Postgres for tables ${SYNC_TABLES}"
