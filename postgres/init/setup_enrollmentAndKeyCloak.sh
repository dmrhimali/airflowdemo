#!/bin/bash
set -e 
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE role rewards_user password 'a1111111' login;
    CREATE DATABASE keycloak;
    CREATE DATABASE enrollment;
    CREATE DATABASE data_ingestion;
    CREATE DATABASE rewards owner rewards_user;
EOSQL

