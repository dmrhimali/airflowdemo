# A really good tutorial:
https://williamqliu.github.io/2018/02/20/apache-airflow.html

# Install Apache Airflow

Ref: [apache airflow](https://airflow.apache.org/start.html)

The installation is quick and straightforward.

Create a new Anaconda python 3.6 environment 'airflowEnv' .

In terminal navigate to envornment:

```bash
RBH12282:~ rdissanayakam$ source activate airflowEnv
(airflowEnv) RBH12282:~ rdissanayakam$ 
```


`
export AIRFLOW_GPL_UNIDECODE=yes
`

Airflow needs a home, ~/airflow is the default,
but you can lay foundation somewhere else if you prefer
(optional):

`
export AIRFLOW_HOME=~/airflow
`

install from pypi using pip:

`pip install apache-airflow`

Check version:

```bash
(airflowEnv) RBH12282:dags rdissanayakam$ airflow version
[2018-10-04 11:24:28,888] {__init__.py:51} INFO - Using executor SequentialExecutor
  ____________       _____________
 ____    |__( )_________  __/__  /________      __
____  /| |_  /__  ___/_  /_ __  /_  __ \_ | /| / /
___  ___ |  / _  /   _  __/ _  / / /_/ /_ |/ |/ /
 _/_/  |_/_/  /_/    /_/    /_/  \____/____/|__/
   v1.10.0

```
Now install boto3:

`pip install boto3`

initialize the database:

`airflow initdb`

output:
```bash
RBH12282:airflow rdissanayakam$ airflow initdb
[2018-10-04 10:35:29,520] {__init__.py:51} INFO - Using executor SequentialExecutor
DB: sqlite:////Users/rdissanayakam/airflow/airflow.db
. . .
```

You should have airflow folder in your home: Users/rdissanayakam/airflow

start the web server, default port is 8080  (if occupied try 8081)

`airflow webserver -p 8080`

Note : if you stop server (CMD+C) , check if the port is hanging and if so kill:

```bash
(airflowEnv) RBH12282:~ rdissanayakam$ lsof -i tcp:8081
(airflowEnv) RBH12282:~ rdissanayakam$ kill -9 <pid>
```

##Run:
to run the dag,  start the scheduler

`airflow scheduler`

visit http://localhost:8080 in the browser and enable the example dag in the home page
![localhost](img/localhost.png " localhost")

Out of the box, Airflow uses a sqlite database, which you should outgrow fairly quickly since no parallelization is possible using this database backend

Here are a few commands that will trigger a few task instances. You should be able to see the status of the jobs change in the example1 DAG as you run the commands below.


run your first task instance:

`airflow run example_bash_operator runme_0 2015-01-01`

run a backfill over 2 days:

`airflow backfill example_bash_operator -s 2015-01-01 -e 2015-01-02`

![dag](img/dag.png " dag")

# Airflow Tutorial

Ref: https://airflow.apache.org/tutorial.html

tutorial.py:

```python
"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2015, 6, 1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG(
    'tutorial', default_args=default_args, schedule_interval=timedelta(1))

# t1, t2 and t3 are examples of tasks created by instantiating operators
t1 = BashOperator(
    task_id='print_date',
    bash_command='date',
    dag=dag)

t2 = BashOperator(
    task_id='sleep',
    bash_command='sleep 5',
    retries=3,
    dag=dag)

templated_command = """
    {% for i in range(5) %}
        echo "{{ ds }}"
        echo "{{ macros.ds_add(ds, 7)}}"
        echo "{{ params.my_param }}"
    {% endfor %}
"""

t3 = BashOperator(
    task_id='templated',
    bash_command=templated_command,
    params={'my_param': 'Parameter I passed in'},
    dag=dag)

t2.set_upstream(t1)
t3.set_upstream(t1)
```

The above file needs to be save in the location specified in airflow.cfg. 


```bash
(airflowEnv) RBH12282:airflow rdissanayakam$ ls
airflow.cfg	airflow.db	logs		unittests.cfg
(airflowEnv) RBH12282:airflow rdissanayakam$ pwd
/Users/rdissanayakam/airflow
```

Content of airflow.cfg shows:

```shell
dags_folder = /Users/rdissanayakam/airflow/dags
```


so create a dag directory inside and save your tutorials.py file in that location:

```bash
(airflowEnv) RBH12282:airflow rdissanayakam$ mkdir dags
(airflowEnv) RBH12282:airflow rdissanayakam$ ls
airflow.cfg	airflow.db	dags		logs		unittests.cfg
(airflowEnv) RBH12282:airflow rdissanayakam$ cd dags/
(airflowEnv) RBH12282:dags rdissanayakam$ vi tutorial.py
```

# Testing

## Running the Script

```bash
(airflowEnv) RBH12282:dags rdissanayakam$ pwd
/Users/rdissanayakam/airflow/dags
(airflowEnv) RBH12282:dags rdissanayakam$ ls
tutorial.py
(airflowEnv) RBH12282:dags rdissanayakam$ python tutorial.py 
(airflowEnv) RBH12282:dags rdissanayakam$ ls
tutorial.py
(airflowEnv) RBH12282:dags rdissanayakam$
```

If the script does not raise an exception it means that you haven’t done anything horribly wrong, and that your Airflow environment is somewhat sound.

## Command Line Metadata Validation
Let’s run a few commands to validate this script further.

```bash
# print the list of active DAGs
airflow list_dags
```

output:

```bash
RBH12282:dags rdissanayakam$ airflow list_dags
[2018-10-04 09:35:39,774] {__init__.py:57} INFO - Using executor SequentialExecutor
[2018-10-04 09:35:40,324] {models.py:167} INFO - Filling up the DagBag from /Users/rdissanayakam/airflow/dags
[2018-10-04 09:35:40,576] {example_kubernetes_operator.py:54} WARNING - Could not import KubernetesPodOperator: No module named 'kubernetes'
[2018-10-04 09:35:40,576] {example_kubernetes_operator.py:55} WARNING - Install kubernetes dependencies with:     pip install airflow['kubernetes']
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'image': 'airflow/ci:latest'}}}
  category=PendingDeprecationWarning
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'image': 'airflow/ci_zip:latest'}}}
  category=PendingDeprecationWarning
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'request_memory': '128Mi', 'limit_memory': '128Mi'}}}
  category=PendingDeprecationWarning


-------------------------------------------------------------------
DAGS
-------------------------------------------------------------------
example_bash_operator
example_branch_dop_operator_v3
example_branch_operator
example_http_operator
example_kubernetes_executor
example_passing_params_via_test_command
example_python_operator
example_short_circuit_operator
example_skip_dag
example_subdag_operator
example_subdag_operator.section-1
example_subdag_operator.section-2
example_trigger_controller_dag
example_trigger_target_dag
example_xcom
latest_only
latest_only_with_trigger
test_utils
tutorial

RBH12282:dags rdissanayakam$ 

```

Next try:

```bash
# prints the list of tasks the "tutorial" dag_id
airflow list_tasks tutorial
```

output:

```bash
RBH12282:dags rdissanayakam$ airflow list_tasks tutorial
[2018-10-04 09:38:05,471] {__init__.py:57} INFO - Using executor SequentialExecutor
[2018-10-04 09:38:06,008] {models.py:167} INFO - Filling up the DagBag from /Users/rdissanayakam/airflow/dags
[2018-10-04 09:38:06,240] {example_kubernetes_operator.py:54} WARNING - Could not import KubernetesPodOperator: No module named 'kubernetes'
[2018-10-04 09:38:06,240] {example_kubernetes_operator.py:55} WARNING - Install kubernetes dependencies with:     pip install airflow['kubernetes']
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'image': 'airflow/ci:latest'}}}
  category=PendingDeprecationWarning
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'image': 'airflow/ci_zip:latest'}}}
  category=PendingDeprecationWarning
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'request_memory': '128Mi', 'limit_memory': '128Mi'}}}
  category=PendingDeprecationWarning
print_date
sleep
templated
RBH12282:dags rdissanayakam$ 
```

Then try:
```bash
# prints the hierarchy of tasks in the tutorial DAG
airflow list_tasks tutorial --tree
```

output:

```bash
RBH12282:dags rdissanayakam$ airflow list_tasks tutorial --tree
[2018-10-04 09:46:46,810] {__init__.py:57} INFO - Using executor SequentialExecutor
[2018-10-04 09:46:47,409] {models.py:167} INFO - Filling up the DagBag from /Users/rdissanayakam/airflow/dags
[2018-10-04 09:46:47,637] {example_kubernetes_operator.py:54} WARNING - Could not import KubernetesPodOperator: No module named 'kubernetes'
[2018-10-04 09:46:47,638] {example_kubernetes_operator.py:55} WARNING - Install kubernetes dependencies with:     pip install airflow['kubernetes']
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'image': 'airflow/ci:latest'}}}
  category=PendingDeprecationWarning
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'image': 'airflow/ci_zip:latest'}}}
  category=PendingDeprecationWarning
/anaconda3/lib/python3.6/site-packages/airflow/models.py:1927: PendingDeprecationWarning: Invalid arguments were passed to PythonOperator. Support for passing such arguments will be dropped in Airflow 2.0. Invalid arguments were:
*args: ()
**kwargs: {'executor_config': {'KubernetesExecutor': {'request_memory': '128Mi', 'limit_memory': '128Mi'}}}
  category=PendingDeprecationWarning
<Task(BashOperator): sleep>
    <Task(BashOperator): print_date>
<Task(BashOperator): templated>
    <Task(BashOperator): print_date>
RBH12282:dags rdissanayakam$ 

```

## Testing
Let’s test by running the actual task instances on a specific date. The date specified in this context is an execution_date, which simulates the scheduler running your task or dag at a specific date + time:

commands:

```bash
# command layout: command subcommand dag_id task_id date

# testing print_date
airflow test tutorial print_date 2015-06-01

# testing sleep
airflow test tutorial sleep 2015-06-01
```

output:

```bash
RBH12282:dags rdissanayakam$ airflow test tutorial print_date 2015-06-01
. . .
--------------------------------------------------------------------------------
Starting attempt 1 of 2
--------------------------------------------------------------------------------

[2018-10-04 09:49:45,650] {models.py:1342} INFO - Executing <Task(BashOperator): print_date> on 2015-06-01 00:00:00
[2018-10-04 09:49:45,661] {bash_operator.py:71} INFO - tmp dir root location: 
/var/folders/7f/5mtfzhrj13q1mfhdrpwm47ghxrpl9v/T
[2018-10-04 09:49:45,662] {bash_operator.py:80} INFO - Temporary script location :/var/folders/7f/5mtfzhrj13q1mfhdrpwm47ghxrpl9v/T/airflowtmp4fzu0lu0//var/folders/7f/5mtfzhrj13q1mfhdrpwm47ghxrpl9v/T/airflowtmp4fzu0lu0/print_datekdcpb33k
[2018-10-04 09:49:45,662] {bash_operator.py:81} INFO - Running command: date
[2018-10-04 09:49:45,670] {bash_operator.py:90} INFO - Output:
[2018-10-04 09:49:45,681] {bash_operator.py:94} INFO - Thu Oct  4 09:49:45 CDT 2018
[2018-10-04 09:49:45,682] {bash_operator.py:97} INFO - Command exited with return code 0
RBH12282:dags rdissanayakam$ 
```

```bash
RBH12282:dags rdissanayakam$ airflow test tutorial sleep 2015-06-01
. . .
--------------------------------------------------------------------------------
Starting attempt 1 of 4
--------------------------------------------------------------------------------

[2018-10-04 09:51:18,257] {models.py:1342} INFO - Executing <Task(BashOperator): sleep> on 2015-06-01 00:00:00
[2018-10-04 09:51:18,269] {bash_operator.py:71} INFO - tmp dir root location: 
/var/folders/7f/5mtfzhrj13q1mfhdrpwm47ghxrpl9v/T
[2018-10-04 09:51:18,270] {bash_operator.py:80} INFO - Temporary script location :/var/folders/7f/5mtfzhrj13q1mfhdrpwm47ghxrpl9v/T/airflowtmpsy7tkwbu//var/folders/7f/5mtfzhrj13q1mfhdrpwm47ghxrpl9v/T/airflowtmpsy7tkwbu/sleepdjnqsrkh
[2018-10-04 09:51:18,270] {bash_operator.py:81} INFO - Running command: sleep 5
[2018-10-04 09:51:18,278] {bash_operator.py:90} INFO - Output:
[2018-10-04 09:51:23,290] {bash_operator.py:97} INFO - Command exited with return code 0
RBH12282:dags rdissanayakam$ 
```

Try:

```bash
# testing templated
airflow test tutorial templated 2015-06-01
```

output:

```bash
RBH12282:dags rdissanayakam$ airflow test tutorial templated 2015-06-01
. . .
[2018-10-04 09:57:46,042] {bash_operator.py:81} INFO - Running command: 
    
        echo "2015-06-01"
        echo "2015-06-08"
        echo "Parameter I passed in"
    
        echo "2015-06-01"
        echo "2015-06-08"
        echo "Parameter I passed in"
    
        echo "2015-06-01"
        echo "2015-06-08"
        echo "Parameter I passed in"
    
        echo "2015-06-01"
        echo "2015-06-08"
        echo "Parameter I passed in"
    
        echo "2015-06-01"
        echo "2015-06-08"
        echo "Parameter I passed in"
    
[2018-10-04 09:57:46,050] {bash_operator.py:90} INFO - Output:
[2018-10-04 09:57:46,055] {bash_operator.py:94} INFO - 2015-06-01
[2018-10-04 09:57:46,055] {bash_operator.py:94} INFO - 2015-06-08
[2018-10-04 09:57:46,055] {bash_operator.py:94} INFO - Parameter I passed in
[2018-10-04 09:57:46,055] {bash_operator.py:94} INFO - 2015-06-01
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - 2015-06-08
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - Parameter I passed in
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - 2015-06-01
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - 2015-06-08
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - Parameter I passed in
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - 2015-06-01
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - 2015-06-08
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - Parameter I passed in
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - 2015-06-01
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - 2015-06-08
[2018-10-04 09:57:46,056] {bash_operator.py:94} INFO - Parameter I passed in
[2018-10-04 09:57:46,056] {bash_operator.py:97} INFO - Command exited with return code 0
RBH12282:dags rdissanayakam$ 
```

Note that the airflow test command runs task instances locally, outputs their log to stdout (on screen), doesn’t bother with dependencies, and doesn’t communicate state (running, success, failed, …) to the database. It simply allows testing a single task instance.

## Backfill

Everything looks like it’s running fine so let’s run a backfill. backfill will respect your dependencies, emit logs into files and talk to the database to record status. If you do have a webserver up, you’ll be able to track the progress. airflow webserver will start a web server if you are interested in tracking the progress visually as your backfill progresses.

start web server(optional):

```bash
(airflowEnv) RBH12282:dags rdissanayakam$ airflow webserver -p 8081 --debug &
[1] 75422
(airflowEnv) RBH12282:dags rdissanayakam$ [2018-10-04 11:06:39,115] {__init__.py:51} INFO - Using executor SequentialExecutor
  ____________       _____________
 ____    |__( )_________  __/__  /________      __
____  /| |_  /__  ___/_  /_ __  /_  __ \_ | /| / /
___  ___ |  / _  /   _  __/ _  / / /_/ /_ |/ |/ /
 _/_/  |_/_/  /_/    /_/    /_/  \____/____/|__/
 
Starting the web server on port 8081 and host 0.0.0.0.
[2018-10-04 11:06:39,639] {models.py:258} INFO - Filling up the DagBag from /Users/rdissanayakam/airflow/dags
[2018-10-04 11:06:39,684] {example_kubernetes_operator.py:54} WARNING - Could not import KubernetesPodOperator: No module named 'kubernetes'
[2018-10-04 11:06:39,684] {example_kubernetes_operator.py:55} WARNING - Install kubernetes dependencies with:     pip install airflow['kubernetes']
[2018-10-04 11:06:39,837] {_internal.py:88} INFO -  * Running on http://0.0.0.0:8081/ (Press CTRL+C to quit)
[2018-10-04 11:06:39,838] {_internal.py:88} INFO -  * Restarting with stat
[2018-10-04 11:06:40,419] {__init__.py:51} INFO - Using executor SequentialExecutor
  ____________       _____________
 ____    |__( )_________  __/__  /________      __
____  /| |_  /__  ___/_  /_ __  /_  __ \_ | /| / /
___  ___ |  / _  /   _  __/ _  / / /_/ /_ |/ |/ /
 _/_/  |_/_/  /_/    /_/    /_/  \____/____/|__/
 
Starting the web server on port 8081 and host 0.0.0.0.
[2018-10-04 11:06:40,879] {models.py:258} INFO - Filling up the DagBag from /Users/rdissanayakam/airflow/dags
[2018-10-04 11:06:40,926] {example_kubernetes_operator.py:54} WARNING - Could not import KubernetesPodOperator: No module named 'kubernetes'
[2018-10-04 11:06:40,926] {example_kubernetes_operator.py:55} WARNING - Install kubernetes dependencies with:     pip install airflow['kubernetes']
[2018-10-04 11:06:41,065] {_internal.py:88} WARNING -  * Debugger is active!
[2018-10-04 11:06:41,086] {_internal.py:88} INFO -  * Debugger PIN: 326-838-371
```

Run:

```bash
# start your backfill on a date range
airflow backfill tutorial -s 2015-06-01 -e 2015-06-07
```

![tutorial](img/tutorial.png " tutorial")

References:
 - https://github.com/mikeghen/airflow-tutorial
shows database set up and move data from 5 mysql databases to gcp

## Database Setup
According to the Airflow Documentation:
>If you want to take a real test drive of Airflow, you should consider setting up a real database backend and switching to the LocalExecutor.
From above link:

I decided I would just install Postgres on my Airflow instance (Ubuntu 16):
```
sudo apt-get install postgresql postgresql-contrib
pip install psycopg2
```
Then to create a user for airflow:
```
$ sudo -u postgres createuser --interactive
Enter name of role to add: airflow
Shall the new role be a superuser? (y/n) n
Shall the new role be allowed to create databases? (y/n) n
Shall the new role be allowed to create more new roles? (y/n) n
```
Then set the user's password and create the database:
```
sudo -u postgres psql
psql (9.5.7)
Type "help" for help.

postgres=# ALTER USER airflow WITH PASSWORD 'airflow_password';
ALTER ROLE
postgres=# CREATE DATABASE airflow;
CREATE DATABASE
```
Next, edit the `airflow.cfg` to use Postgres by adding:
```
# The Postgres connection string
sql_alchemy_conn = postgresql://airflow:airflow_password@localhost/airflow
```
and comment out the SQLite config.

Finally, reinitialize the database:
```
airflow initdb
```
### Restarting Airflow
 I had to restart Airflow which wasn't as simple as I expected. I ended up using `kill -9` to kill all the airflow processes. I tried other solutions posted on Stack Overflow, but eventually just killed the processes using `-9` and restarted using:
```
airflow webserver -p 8080 -D
```
:warning: You should really configure systemd

## Starting the Scheduler
The scheduler needs to be running in order for jobs and tasks to be executed. To start the scheduler, run:
```
airflow scheduler
```

## Integration with systemd
:pencil: http://pythonhosted.org/airflow/configuration.html#integration-with-systemd


Ref: 
  - http://soverycode.com/airflow-in-production-a-fictional-example/
  productionalizing airflow

[Video](https://youtu.be/iTg-a4icf_I)

Ref: 
  - https://github.com/bloomberg/airflow/blob/master/tests/hooks/test_s3_hook.py
  Testing hooks

Ref:
  -https://github.com/airflow-plugins/Example-Airflow-DAGs
  
This repository contains example DAGs that can be used "out-of-the-box" using operators found in the Airflow Plugins organization. These DAGs have a range of use cases and vary from moving data (see ETL) to background system automation that can give your Airflow "super-powers".

Ref:
  - https://programtalk.com/python-examples/airflow.hooks.mysql_hook.MySqlHook/
  mysql hook

Ref:
  - https://programtalk.com/python-examples/?api=airflow
  airflow examples programtalk

# Packaging DAGs

Since we will be creating subfolders under dag, the __init__.py file needs to be placed in every subfolder
to auto-discover modules.

For an example on how to import a file in a dag subfolder check  fileToS3/localFileToS3Airflow.py
