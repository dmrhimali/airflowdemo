# Save data from a database in S3

## Create a postgres database foo and create a table link with data
1. export AIRFLOW_HOME=~/airflow

1. start a postgres docker container.

    ```
    RBH12282:git rdissanayakam$ docker run --name foo -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_DB=foo -e POSTGRES_PASSWORD=123456 -d postgres
    ```

1. Connect to database using DbVisualizer


1. Create a table link and insert data using createDb.sql

![createLinkTable](../img/createLinkTable.png " createLinkTable")

![linkTable](../img/linkTable.png " linkTable")

## Activate airflowEnv virtual environment created in previous project

```bash
RBH12282:queryDbToFile rdissanayakam$ source activate airflowEnv
(airflowEnv) RBH12282:queryDbToFile rdissanayakam$

(airflowEnv) RBH12282:queryDbToFile rdissanayakam$ pwd
/Users/rdissanayakam/DEV/bitbucket/git/airflowdemo/queryDbToFile

(airflowEnv) RBH12282:queryDbToFile rdissanayakam$ ls
README.md	createDb.sql	dbQuery.py
```

## Install postgres python library

```bash
(airflowEnv) RBH12282:queryDbToFile rdissanayakam$ pip install psycopg2
```

## In your test folder create dbQuery.py python file

test it locally

In the source folder run the program:

```bash
(airflowEnv) RBH12282:queryDbToFile rdissanayakam$ python dbQuery.py 

1 https://www.postgresql.org PostgreSQL
2 http://www.oreilly.com O'Reilly Media
3 http://www.google.com Google
4 http://www.yahoo.com Yahoo
5 http://www.bing.com Bing
```

## Place __init__.py file in dbToS3 folder to allow importing sub modules.

## Copy dbToS3 to airflow home(/Users/rdissanayakam/airflow/dags/dbToS3) , compile program, start scheduler and run web server
`$ python dbtoS3Dag.py`

`$ airflow scheduler`

`$ airflow webserver -p 8081`

In webserver page trigger job and wait till succeed.

![dag](../img/dbtos3.png " dag")

you can verify links.csv was uploaded successfully in aws s3 console.