DROP TABLE IF EXISTS link;

CREATE TABLE link (
 ID serial PRIMARY KEY,
 url VARCHAR (255) NOT NULL,
 name VARCHAR (255) NOT NULL,
 description VARCHAR (255)
);

INSERT INTO link (url, name, description)
VALUES
 ('https://www.postgresql.org','PostgreSQL', 'postgres site' ),
 ('http://www.oreilly.com','O''Reilly Media', 'oreilly site'),
 ('http://www.google.com','Google', 'google site'),
 ('http://www.yahoo.com','Yahoo', 'yahoo site'),
 ('http://www.bing.com','Bing', 'bing site');