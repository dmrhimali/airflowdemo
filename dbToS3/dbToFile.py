#!/usr/bin/python
import psycopg2
import pandas as pd

hostname = 'localhost'
username = 'postgres'
password = '123456'
database = 'foo'

# Simple routine to run a query on a database and print the results:
def doQuery( ) :
    conn = psycopg2.connect( host=hostname, user=username, password=password, dbname=database )
    sql="SELECT id, url, name FROM link"
    file_name="links.csv"

    cur = conn.cursor()
    cur.execute(sql)

    for id, url, name in cur.fetchall() :
        print (id, url, name)
    
    #write result to csv
    df = pd.read_sql_query(sql, con=conn)
    df.to_csv(file_name, sep=',', index=False)

    conn.close()

#myConnection = psycopg2.connect( host=hostname, user=username, password=password, dbname=database )
#doQuery( myConnection )
#myConnection.close()
