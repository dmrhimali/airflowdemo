import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta
from io import StringIO
import boto3
import os
from dbToS3.dbToFile import doQuery
from dbToS3.fileToS3 import uploadToS3


# these args will get passed on to each operator
# you can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'dbToS3',
    default_args=default_args,
    description='A db to s3 DAG',
    schedule_interval=timedelta(days=1))

t1 = PythonOperator(dag=dag,
               task_id='python_db_to_csv',
               provide_context=False,
               python_callable=doQuery
               )

t2 = PythonOperator(dag=dag,
               task_id='python_csv_upload_to_s3',
               provide_context=False,
               python_callable=uploadToS3
               )

t1 >> t2

