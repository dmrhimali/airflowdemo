#!/bin/sh
JDBC_POSTGRES_VERSION="42.2.4"
KEYCLOAK_HOME="/opt/jboss/keycloak"

echo "JDBC_POSTGRES_VERSION=${JDBC_POSTGRES_VERSION}"
echo "KEYCLOAK_HOME=${KEYCLOAK_HOME}"

echo "===== Installing postgres driver module ====="
if [ ! -d "${KEYCLOAK_HOME}/modules/system/layers/base/org/postgresql/jdbc/main" ]; then
    mkdir -p "${KEYCLOAK_HOME}/modules/system/layers/base/org/postgresql/jdbc/main"
fi

if [ ! -f ${KEYCLOAK_HOME}/modules/system/layers/base/org/postgresql/jdbc/main/postgresql-${JDBC_POSTGRES_VERSION}.jar ]; then
    echo "Downloading keycloak postgresql adapter..."
    curl -L http://central.maven.org/maven2/org/postgresql/postgresql/${JDBC_POSTGRES_VERSION}/postgresql-${JDBC_POSTGRES_VERSION}.jar > ${KEYCLOAK_HOME}/modules/system/layers/base/org/postgresql/jdbc/main/postgresql-${JDBC_POSTGRES_VERSION}.jar
fi

if [ ! -f /tmp/entrypoint/migrationsRan.txt ]; then
	/bin/echo 'finished' > /tmp/entrypoint/migrationsRan.txt
	exec /opt/jboss/keycloak/bin/standalone.sh -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=dir -Dkeycloak.migration.dir=/tmp/keycloak/ -Dkeycloak.migration.strategy=OVERWRITE_EXISTING --debug 8787;
fi

if [ $KEYCLOAK_USER ] && [ $KEYCLOAK_PASSWORD ]; then
    keycloak/bin/add-user-keycloak.sh --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD $@
fi

exec /opt/jboss/keycloak/bin/standalone.sh --debug 8787 $@
exit $?