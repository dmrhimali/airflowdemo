# Local File copy to S3 bucket

## Prerequesites

You need airflowEnv anaconda environment active and airflow and boto3 installed

Then export airflow home:

`export AIRFLOW_HOME=~/airflow`

**Copy file localFileToS3AirFlow and example.csv to a subfolder fileToS3 in airflow home dags folder.**

```bash
RBH12282:fileToS3 rdissanayakam$ pwd
/Users/rdissanayakam/airflow/dags/fileToS3
RBH12282:fileToS3 rdissanayakam$ ls
__pycache__		example.csv		localFileToS3AirFlow.py
RBH12282:fileToS3 rdissanayakam$
```

**Now compile and veirfy dag is created:**

```bash

(airflowEnv) RBH12282:fileToS3 rdissanayakam$ python localFileToS3AirFlow.py
(airflowEnv) RBH12282:fileToS3 rdissanayakam$ airflow list_dags
-------------------------------------------------------------------
DAGS
-------------------------------------------------------------------
example_bash_operator
. . .
fileToS3

```

**start the scheduler**

```bash
(airflowEnv) RBH12282:fileToS3 rdissanayakam$ airflow scheduler
[2018-10-04 13:32:47,184] {__init__.py:51} INFO - Using executor SequentialExecutor
____________       _____________
____    |__( )_________  __/__  /________      __
____  /| |_  /__  ___/_  /_ __  /_  __ \_ | /| / /
___  ___ |  / _  /   _  __/ _  / / /_/ /_ |/ |/ /
_/_/  |_/_/  /_/    /_/    /_/  \____/____/|__/

[2018-10-04 13:32:47,404] {jobs.py:580} ERROR - Cannot use more than 1 thread when using sqlite. Setting max_threads to 1
[2018-10-04 13:32:47,414] {jobs.py:1541} INFO - Starting the scheduler
. . .
```

**In another terminal run webserver (you may want to check and kill hanging process on webserver port before this):**

```bash
(airflowEnv) RBH12282:fileToS3 rdissanayakam$ airflow webserver -p 8081
[2018-10-04 13:35:48,673] {__init__.py:51} INFO - Using executor SequentialExecutor
____________       _____________
____    |__( )_________  __/__  /________      __
____  /| |_  /__  ___/_  /_ __  /_  __ \_ | /| / /
___  ___ |  / _  /   _  __/ _  / / /_/ /_ |/ |/ /
_/_/  |_/_/  /_/    /_/    /_/  \____/____/|__/

...
Running the Gunicorn Server with:
Workers: 4 sync
Host: 0.0.0.0:8081
Timeout: 120
Logfiles: - -
=================================================================            
[2018-10-04 13:35:50 -0500] [82545] [INFO] Starting gunicorn 19.9.0
[2018-10-04 13:35:50 -0500] [82545] [INFO] Listening at: http://0.0.0.0:8081 (82545)
```

**Now open the webserver ui, to start a DAG Run, first turn the workflow on , then click the Trigger Dag button (Trigger Dag) and finally, click on the Graph View to see the progress of the run**
![webmain](../img/fileToS3WebMain.png " webmain")

Graph view:
![graph](../img/graphView.png " graph")


**You can reload the graph view until the task reach the status Success. When  it is done, you can click on the hello_task and then click View Log. If everything worked as expected, the log should show a number of lines and among them something like this:**

![log](../img/log.png " log")



**Now login to your aws cs3 console and verify file is uploaded:**

![s3](../img/s3bucket.png " s3")


**Optinally, importing and calling a python function also works if path to the external python file to be executed is added to PYTHONPATH.**

localFileToS3Boto.py in same folder as localFileToS3Airflow:

```python
from io import StringIO
import boto3
import os

def uploadToS3():
    data = open(os.path.abspath('example.csv'), 'rb')
    s3 = boto3.resource('s3')
    s3.Bucket('rbhreports').put_object(Key='example.csv', Body=data)
    print("Uploaded file!")
```


Change your localFileToS3Airflow.py as follows:

```python
import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta
from io import StringIO
import boto3
import os
from localFileToS3Boto import uploadToS3

def copyLocalFileToS3():
    data = open('example.csv', 'rb')
    s3 = boto3.resource('s3')
    s3.Bucket('rbhreports').put_object(Key='example.csv', Body=data)
    return "Uploaded file!"     #return 'Hello world!'

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}

dag = DAG(
    'fileToS3',
    default_args=default_args,
    description='A simple tutorial DAG',
    schedule_interval=timedelta(days=1))

#works:
#python_task = PythonOperator(task_id='python_upload_to_s3', python_callable=copyLocalFileToS3, dag=dag)

python_task = PythonOperator(dag=dag,
               task_id='python_upload_to_s3',
               provide_context=False,
               python_callable=uploadToS3
               )


#Doesnt work: Needs example.csv in temp folder the script is copied
# fileName =os.path.abspath("localFileToS3Boto.py")
# bash_task = BashOperator(
#     task_id='bash_task',
#     bash_command='python '+fileName,
#     dag=dag
# )
```

After compiling and runnig scheduler and triggering task in webui, file will be uploaded.


# Upload a file to S3 using S3Hook

### Start webserver and add a s3 connection in Admin > Connections:
![s3connection](../img/s3connection.png " s3connection")

:exclamation: 
Note: Extras can be left blank and airflow will find it in  aes credentials: See log message if run a dag without extras:
```
Found credentials in shared credentials file: ~/.aws/credentials
```

Or config file can be pointed to in extras:

[From stack overflow](https://stackoverflow.com/questions/39997714/airflow-s3-connection-using-ui):

>If you are worried about exposing the credentials in the UI, another way is to pass credential file location in the Extra param in UI. Only the functional user has read privileges to the file. It looks something like below

    ```
    Extra:  {
        "profile": "<profile_name>", 
        "s3_config_file": "/home/<functional_user>/creds/s3_credentials", 
        "s3_config_format": "aws" }
    ```

>file "/home/<functional_user>/creds/s3_credentials" has below entries:

```
[<profile_name>]
aws_access_key_id = <access_key_id>
aws_secret_access_key = <secret_key>
```

### Copy localFileToS3Hook.py to airflow_home dags directory:

```bash
RBH12282:fileToS3 rdissanayakam$ ls
__pycache__		example.csv		localFileToS3AirFlow.py	localFileToS3Boto.py	localFileToS3Hook.py
RBH12282:fileToS3 rdissanayakam$ 
```

localFileToS3Hook.py:

```python
import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.S3_hook import S3Hook

from datetime import timedelta
from io import StringIO
import boto3
import os

def checkBucketStatus():
    dest_s3_hook = S3Hook(aws_conn_id='my_conn_S3')
    return dest_s3_hook.check_for_bucket('rbhreports')

def copyLocalFileToS3():
    dest_s3_hook = S3Hook(aws_conn_id='my_conn_S3')
    return dest_s3_hook.load_file(
        filename='example.csv',
        key='example.csv',
        bucket_name='rbhreports',
        replace=False,
        encrypt=False)

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}

dag = DAG(
    'fileToS3Hook',
    default_args=default_args,
    description='A local file copy to S3 DAG',
    schedule_interval=timedelta(days=1))

#works:
python_task_1 = PythonOperator(task_id='python_check_bucket_status_hook', python_callable=copyLocalFileToS3, dag=dag)

python_task_2 = PythonOperator(task_id='python_upload_to_s3_hook', python_callable=checkBucketStatus, dag=dag)


python_task_1 >> python_task_2
```

### Now compile the program in airflow home and start scheduler and webserver:

`$ python localFileToS3Hook.py`

`$ airflow scheduler`

`$ airflow webserver -p 8081`

### In webUI start the task 'fileToS3Hook' and check graph view. Job executes successfully:

![dag](../img/fileToS3HookDag.png " dag")

![log](../img/fileToS3HookLog.png " log")

You should see the example.csv in the s3 bucket in aws console.

## Hooks Tutorials:

Creating operators, sensors, xcom:
http://michal.karzynski.pl/blog/2017/03/19/developing-workflows-with-apache-airflow/

https://github.com/postrational/airflow_tutorial/tree/15bd74b0d513485673b410fd2b7d989a987cc20b/airflow_home

