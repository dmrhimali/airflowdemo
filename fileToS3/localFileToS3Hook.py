import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.S3_hook import S3Hook

from datetime import timedelta
from io import StringIO
import boto3
import os


def checkBucketStatus():
    dest_s3_hook = S3Hook(aws_conn_id='my_conn_S3')
    return dest_s3_hook.check_for_bucket('rbhreports')

def copyLocalFileToS3():
    dest_s3_hook = S3Hook(aws_conn_id='my_conn_S3')
    return dest_s3_hook.load_file(
        filename='example.csv',
        key='example.csv',
        bucket_name='rbhreports',
        replace=False,
        encrypt=False)


# these args will get passed on to each operator
# you can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'fileToS3Hook',
    default_args=default_args,
    description='A local file copy to S3 DAG',
    schedule_interval=timedelta(days=1))

#works:
python_task_1 = PythonOperator(task_id='python_check_bucket_status_hook', python_callable=copyLocalFileToS3, dag=dag)

python_task_2 = PythonOperator(task_id='python_upload_to_s3_hook', python_callable=checkBucketStatus, dag=dag)


python_task_1 >> python_task_2
