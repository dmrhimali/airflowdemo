import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta
from io import StringIO
import boto3
import os
from fileToS3.localFileToS3Boto import uploadToS3

def copyLocalFileToS3():
    data = open('example.csv', 'rb')
    s3 = boto3.resource('s3')
    s3.Bucket('rbhreports').put_object(Key='example.csv', Body=data)
    return "Uploaded file!"     #return 'Hello world!'


# these args will get passed on to each operator
# you can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'fileToS3',
    default_args=default_args,
    description='A simple tutorial DAG',
    schedule_interval=timedelta(days=1))

#works:
#python_task = PythonOperator(task_id='python_upload_to_s3', python_callable=copyLocalFileToS3, dag=dag)

python_task = PythonOperator(dag=dag,
               task_id='python_upload_to_s3',
               provide_context=False,
               python_callable=uploadToS3
               )


#Doesnt work: Needs example.csv in temp folder the script is copied
# fileName =os.path.abspath("localFileToS3Boto.py")
# bash_task = BashOperator(
#     task_id='bash_task',
#     bash_command='python '+fileName,
#     dag=dag
# )


