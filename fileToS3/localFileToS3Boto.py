from io import StringIO
import boto3
import os

def uploadToS3():
    data = open(os.path.abspath('example.csv'), 'rb')
    s3 = boto3.resource('s3')
    s3.Bucket('rbhreports').put_object(Key='example.csv', Body=data)
    print("Uploaded file!")


'''
AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
AWS_BUCKET_NAME = ''
 
 
import boto
import sys
from boto.s3.key import Key
 
 
def _connect_to_s3():
    s3_conn = boto.connect_s3(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
    return s3_conn
 
 
def _get_bucket():
    s3_conn = _connect_to_s3()
    bucket = s3_conn.get_bucket(AWS_BUCKET_NAME)
    return bucket
 
 
def _upload_file_by_path(file_path, file_key=None):
    def percent_cb(complete, total):
        sys.stdout.write('.')
        sys.stdout.flush()
 
    if file_key is None:
        file_key = file_path
 
    bucket = _get_bucket()
    key = Key(bucket)
    key.key = file_key
    key.set_contents_from_filename(file_path, cb=percent_cb)
 
 
def _get_file_by_key(file_key, file_name=None):
    if file_name is None:
        file_name = file_key
 
    bucket = _get_bucket()
    key = Key(bucket)
    key.key = file_key
    key.get_contents_to_filename(file_name)
 
 
file_key = 'kasatka.jpg'
#_upload_file_by_path(file_name)
_get_file_by_key(file_key, 'save_to_file.jpg')
'''